#!/bin/bash

###############################################################################
# Maintainer: Cosku Bas <info@wirevpn.net>
#
# This script can be used to prepare a Wireguard VPN server with WireVPN's
# custom supervisor software.
#
# Attention:
#   Tested only with Ubuntu 18.04 and Debian 9
#   Contains sensitive information. Do not commit to a public repo!
###############################################################################

# variables
export GOROOT=/usr/local/go
export GOPATH=/opt/go
export PATH=$GOROOT/bin:$PATH:/root/speedtest/wireguard-go:/root/speedtest
echo 'export GOROOT=/usr/local/go' >> /root/.bashrc
echo 'export GOPATH=/opt/go' >> /root/.bashrc
echo 'export PATH=$GOROOT/bin:$PATH:/root/speedtest/wireguard-go:/root/speedtest' >> /root/.bashrc

# disable interactive menus while upgrading
export DEBIAN_FRONTEND=noninteractive

# install wireguard
echo "deb http://deb.debian.org/debian/ unstable main" > /etc/apt/sources.list.d/unstable-wireguard.list
printf 'Package: *\nPin: release a=unstable\nPin-Priority: 90\n' > /etc/apt/preferences.d/limit-unstable
apt update && apt -y upgrade
apt -y install linux-headers-$(uname -r)
apt -y install wireguard
modprobe wireguard

# setup wireguard
cp configs/*.conf /etc/wireguard/

# install go
wget https://dl.google.com/go/go1.11.4.linux-amd64.tar.gz
tar -xvf go1.11.4.linux-amd64.tar.gz
rm -rf /usr/local/go
mv go /usr/local
rm -rf go1.11.4.linux-amd64.tar*

# build speedtest
go build

# install wireguard-go
git clone https://git.zx2c4.com/wireguard-go
cd wireguard-go
go build
cd /root

# installation done
echo 'DONE' > /root/DONE.txt