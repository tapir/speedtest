package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

var urls = map[string]string{
	"NL [HTTP]":  "http://speedtest-ams2.digitalocean.com/100mb.test",
	"UK [HTTP]":  "http://speedtest.london.linode.com/100MB-london.bin",
	"DE [HTTPS]": "https://speed.hetzner.de/100MB.bin",
}

func main() {
	fmt.Println("Speed test started...")
	for k, v := range urls {
		start := time.Now()
		if err := downloadFile(v); err != nil {
			panic(err)
		}
		delta := time.Now().Sub(start)
		avgSpeed := 100 / (float64(delta) / float64(time.Second))
		fmt.Printf("%v -> Average Speed: %.2f mb/s\n", k, avgSpeed)
	}
}

func downloadFile(url string) error {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(ioutil.Discard, resp.Body)
	return err
}
